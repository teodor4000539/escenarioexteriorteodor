using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class insidedoor1 : MonoBehaviour
{
    public Animator insidedoorone;

    private void OnTriggerEnter(Collider other)
    {
        insidedoorone.Play("CloseInsideDoor");
    }

    private void OnTriggerExit(Collider other)
    {
        insidedoorone.Play("OpenInsideDoorAnimation");
    }
}
