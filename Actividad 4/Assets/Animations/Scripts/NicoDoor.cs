using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NicoDoor : MonoBehaviour
{
    public Animator NicosDoor;

    private void OnTriggerEnter(Collider other)
    {
        NicosDoor.Play("OpenNico");
    }

    private void OnTriggerExit(Collider other)
    {
        NicosDoor.Play("CloseNico");
    }
}
