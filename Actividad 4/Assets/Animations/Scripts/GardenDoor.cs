using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GardenDoor : MonoBehaviour
{
    public Animator gardendoor;

    private void OnTriggerEnter(Collider other)
    {
        gardendoor.Play("CloseGarden");
    }

    private void OnTriggerExit(Collider other)
    {
        gardendoor.Play("OpenGarden");
    }
}
