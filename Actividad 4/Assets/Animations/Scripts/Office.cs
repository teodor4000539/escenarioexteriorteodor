using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Office : MonoBehaviour
{
    public Animator office;
    private void OnTriggerEnter(Collider other)
    {
        office.Play("OfficeDoorOpen");
    }

    private void OnTriggerExit(Collider other)
    {
        office.Play("OfficeDoorClose");
    }
}
