using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fixed3 : MonoBehaviour
{
    public Animator FixedDoor3;

    private void OnTriggerEnter(Collider other)
    {
        FixedDoor3.Play("Open3");
    }

    private void OnTriggerExit(Collider other)
    {
        FixedDoor3.Play("Close3");
    }
}
