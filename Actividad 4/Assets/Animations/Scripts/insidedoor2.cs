using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class insidedoor2 : MonoBehaviour
{
    public Animator insidedoortwo;

    private void OnTriggerEnter(Collider other)
    {
        insidedoortwo.Play("OpenInsideDoorAnimation2");
    }

    private void OnTriggerExit(Collider other)
    {
        insidedoortwo.Play("CloseInsideDoor2");
    }
}
