using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fixed2 : MonoBehaviour
{
    public Animator FixedDoor2;

    private void OnTriggerEnter(Collider other)
    {
        FixedDoor2.Play("Open2");
    }

    private void OnTriggerExit(Collider other)
    {
        FixedDoor2.Play("Close2");
    }
}
