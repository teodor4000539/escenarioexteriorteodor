using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fixed5 : MonoBehaviour
{
    public Animator FixedDoor5;

    private void OnTriggerEnter(Collider other)
    {
        FixedDoor5.Play("Open5");
    }

    private void OnTriggerExit(Collider other)
    {
        FixedDoor5.Play("Close5");
    }
}
