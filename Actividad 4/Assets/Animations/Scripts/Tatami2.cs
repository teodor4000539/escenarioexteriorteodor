using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tatami2 : MonoBehaviour
{
    public Animator tatamitwo;
    private void OnTriggerEnter(Collider other)
    {
        tatamitwo.Play("TatamiEntereance2Close");
    }

    private void OnTriggerExit(Collider other)
    {
        tatamitwo.Play("TatamiEntarance2");
    }
}
