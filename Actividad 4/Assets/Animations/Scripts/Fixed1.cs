using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fixed1 : MonoBehaviour
{
    public Animator FixedDoor1;

    private void OnTriggerEnter(Collider other)
    {
        FixedDoor1.Play("Open1");
    }

    private void OnTriggerExit(Collider other)
    {
        FixedDoor1.Play("Close1");
    }
}
