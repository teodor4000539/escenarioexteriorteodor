using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutsideDoor : MonoBehaviour
{

    
    public Animator outsidedoor;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            outsidedoor.SetBool("Open", true);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player")) 
            outsidedoor.SetBool("Close", true);
    }
    

   /*
    Animator outsidedoor;

    void Start()
    {
        outsidedoor = gameObject.GetComponent<Animator>();

        outsidedoor = false;
    }

    private void Update()
    {
        
        if (InputG)
        {
            outsidedoor = true;
        }
        else
        {
            outsidedoor = false;
        }
        

        if (outsidedoor == false)
        {
            outsidedoor.SetBool("Open", false);
        }

        if (outsidedoor == true)
        {
            outsidedoor.SetBool("Open", true);
        }
        
    }

    */
}
