using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fixed4 : MonoBehaviour
{
    public Animator FixedDoor4;

    private void OnTriggerEnter(Collider other)
    {
        FixedDoor4.Play("Open4");
    }

    private void OnTriggerExit(Collider other)
    {
        FixedDoor4.Play("Close4");
    }
}
