using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tatami1 : MonoBehaviour
{
    public Animator tatamione;
    private void OnTriggerEnter(Collider other)
    {
        tatamione.Play("TatamiEnterance1Close");
    }

    private void OnTriggerExit(Collider other)
    {
        tatamione.Play("TatamiEnterance1");
    }
}
