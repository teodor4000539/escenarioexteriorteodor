using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fixed6 : MonoBehaviour
{
    public Animator FixedDoor6;

    private void OnTriggerEnter(Collider other)
    {
        FixedDoor6.Play("Open6");
    }

    private void OnTriggerExit(Collider other)
    {
        FixedDoor6.Play("Close6");
    }
}
