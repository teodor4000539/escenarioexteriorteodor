using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fixed7 : MonoBehaviour
{
    public Animator FixedDoor7;

    private void OnTriggerEnter(Collider other)
    {
        FixedDoor7.Play("Open7");
    }

    private void OnTriggerExit(Collider other)
    {
        FixedDoor7.Play("Close7");
    }
}
